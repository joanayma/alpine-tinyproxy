alpine/tinyproxy
================

A container of a fast and tiny proxy.

 - Port: 3128
 - getting stats:
   - curl: `http_proxy=localhost:3128 curl -f http://tinyproxy.stats`

Example
-------

 - docker-compose.yml:

```yaml
services:
  proxy:
    container_name: proxy
    image: registry.gitlab.com/joanayma/alpine-tinyproxy
    restart: always
```
