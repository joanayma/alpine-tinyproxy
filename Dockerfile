FROM alpine
LABEL maintainer="Joan Aymà <joan.ayma@gmail.com>"

RUN apk add --no-cache tinyproxy curl rsyslog
COPY files/rsyslog.conf /etc/rsyslog.conf
COPY files/tinyproxy.conf /etc/tinyproxy/tinyproxy.conf
HEALTHCHECK --retries=3 --interval=5s --timeout=3s \
  CMD http_proxy=localhost:3128 curl -f http://tinyproxy.stats > /dev/null
CMD ["sh", "-c", "rsyslogd -n & tinyproxy -d"]
